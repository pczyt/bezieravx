![picture](screenshoot/1.png)
---
Funkcja rysująca przyjmuje jako argumenty:

1. unsigned char *pPixelBuffer - wskaźnik na początek obszaru przeznaczonego na dane o pikselach
2. float *cords - tablica współrzędnych punktów, ułożonych w odpowiedniej kolejności
3. int iterations - żądana liczba iteracji
4. long* dim - tablica trójelementowa zawierająca wysokość, szerokość obrazka oraz szerokość bajtową wiersza
---
Współrzędne punktów w tablicy cords muszą być ułożone w zadanej kolejności:

* 0 - 7 kolejno elementy x0 - x7
* 8 - 16 kolejno elementy y0 - y7
taka kolejność rozmieszczenia elementów pozwala na bezpośrednie załadowanie całych zestawów współrzędnych do rejestrów, bez konieczności zamieniania ich kolejności.