%use altreg
        section .text ; program 64-bitowy
        global drawInMemory

drawInMemory:
    push r12
    push r13
    push r14
    vzeroall
    vmovaps ymm0, [rsi]             ; laduje x0-x7
    vmovaps ymm1, [rsi+32]          ; laduje y0-y7
    mov r10, coefs                  ; laduje adres wektora wspolczynnikow
    vmovaps ymm2, [r10]             ; laduje wektor wspolczynnikow
    mov r8, rdx                     ; liczba iteracji
    test r8, r8
    jz end
    mov r9, [rcx]                   ; szerokosc
    mov r10, [rcx+8]                ; wysokosc
    dec r9
    dec r10
    mov r13, [rcx+16]               ; szerokosc bajtowa wiersza
    mov r12, 1
    cvtsi2ss xmm11, r12             ; 1 float
    movss xmm13, xmm11              ; 1 float kopia
    cvtsi2ss xmm14, r9              ; szerokosc float
    cvtsi2ss xmm15, r10             ; wysokosc float
    cvtsi2ss xmm5, r8               ; ilosc iteracji
    divss xmm11, xmm5               ; 1/ilosc iteracji - krok petli
    pxor xmm8, xmm8                 ; t


drawLoop:
    ;przygotowanie wektora poteg t

    movss xmm3, xmm8
    VPERM2F128 ymm3, ymm3, 0x0
    VPERMILPS ymm3, ymm3, 0x0

    movss xmm3, xmm13               ; wektor1[t,t,t,t,t,t,t,1]
    VPERM2F128 ymm3, ymm3, ymm3, 0x0; wektor1[t,t,t,1,t,t,t,1]
    shufps xmm3, xmm3, 0xe0         ; wektor1[t,t,t,1,t,t,1,1]
    VPERMILPS ymm4, ymm3, 0x59      ; wektor2[t,t,t,t,1,1,t,1]
    VMULPS ymm3, ymm3, ymm4         ; wektor1[t^2,t^2,t^2,t,t,t,t,1]
    VPERMILPS ymm4, ymm3, 0x50      ; wektor2[t^2,t^2,t,t,t,t,1,1]
    VMULPS ymm3, ymm3, ymm4         ; wektor1[t^4,t^4,t^3,t^2,t^2,t^2,t,1]
    VPERMILPS ymm4, ymm3, 0x40      ; wektor2[t^3,t^2,t^2,t^2,t,1,1,1]
    VMUlPS ymm3, ymm3, ymm4         ; wektor1[t^7,t^6,t^5,t^4,t^3,t^2,t,1]

    ;przygotowanie wektora poteg (1-t), schemat jak wyzej
    movss xmm12, xmm13              ; 1 float
    subss xmm12, xmm8               ; 1-t
    movss xmm6, xmm12
    VPERM2F128 ymm6, ymm6, 0x0
    VPERMILPS ymm6, ymm6, 0x0
    movss xmm6, xmm13
    VPERM2F128 ymm6, ymm6, 0x0
    shufps xmm6, xmm6, 0xe0
    VPERMILPS ymm7, ymm6, 0x59
    VMULPS ymm6, ymm6, ymm7
    VPERMILPS ymm7, ymm6, 0x50
    VMULPS ymm6, ymm6, ymm7
    VPERMILPS ymm7, ymm6, 0x40
    VMUlPS ymm6, ymm6, ymm7         ; wektor w postaci [(1-t)^7, (1-t)^6... trzeba odwrocic
    VPERM2F128 ymm6, ymm6, 0x01     ; zamienione polowki wektora poteg (1-t)
    VPERMILPS  ymm6, ymm6, 0x1b     ; zamieniona kolejnosc elementow, teraz wektor jest wg rosnacych poteg
    VMULPS ymm3, ymm3, ymm6         ; wektor poteg t * wektor poteg (1-t)
    VMULPS ymm3, ymm2               ; wektor wyliczony = wektor powyzszy * wektor wspolczynnikow
    VMULPS ymm4, ymm3, ymm0         ; wektor x * wektor wyliczony
    VMULPS ymm5, ymm3, ymm1         ; wektor y * wektor wyliczony
    VHADDPS  ymm4, ymm4, ymm4       ; sumuje horyzontalnie wyliczony wektor xow
    VHADDPS  ymm4, ymm4, ymm4       ; po drugim razie pierwsze elementu obu czworek sa suma wyjsciowych czworek
    VEXTRACTF128 xmm12, ymm4, 0x1   ; kopiuje sume lewej polowki do rejestru tymczasowego
    ADDSS   xmm4, xmm12             ; sumuje sumy obu polowek, otrzymuje sume przeliczonego wektora xow
    VHADDPS  ymm5, ymm5, ymm5       ; sumuje horyzontalnie wyliczony wektor yow
    VHADDPS  ymm5, ymm5, ymm5       ; po drugim razie pierwsze elementu obu czworek sa suma wyjsciowych czworek
    VEXTRACTF128 xmm10, ymm5, 0x1   ; kopiuje sume lewej polowki do rejestru tymczasowego
    ADDSS   xmm10, xmm5             ; sumuje sumy obu polowek, otrzymuje sume przeliczonego wektora yow

    ;xmm4 - policzone dla x
    ;xmm10 - policzone dla y

    mulss xmm4, xmm14               ; przeskalowany x(*szerokosc obrazka)
    roundss xmm4, xmm4, 9           ; zaokraglenie x
    cvttss2si r11, xmm4             ; x calkowity

    mulss xmm10, xmm15              ; przeskalowany y (*wysokosc obrazka)
    roundss xmm10, xmm10, 0x03      ; zaokraglenie y
    cvttss2si r12, xmm10            ; y calkowity
    mov r14, r11
    shl r11, 1                      ; x razy 3 cz.1
    add r11, r14                    ; x razy 3 cz.2
    imul r12, r13                   ; y*szerokosc bajtowa wiersza
    add  r12, r11                   ;calkowite przesuniecie w bitmapie
    add r12, rdi                    ;przesuniecie o poczatek bitmapy

    ;kolorowanie
    mov BYTE [r12], 0xff
    mov BYTE [r12+1], 0xff
    mov BYTE [r12+2], 0xff

    addss xmm8, xmm11 ; t+krok
    comiss xmm8, xmm13
    jb drawLoop


end:

    pop r14
    pop r13
    pop r12


    ret

section .data align=32
coefs: dd 1.0, 7.0, 21.0, 35.0, 35.0, 21.0, 7.0, 1.0

